package ru.semenov.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.domain.entity.UrlMapping;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UrlMappingResponseWithOriginalUrl {
    @NotBlank
    private String originalUrl;
    public UrlMappingResponseWithOriginalUrl(UrlMapping urlMapping){
        this.originalUrl = urlMapping.getOriginalUrl();
    }
}
