package ru.semenov.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.domain.entity.UrlMapping;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UrlMappingResponseWithShortUrl {
    @NotBlank
    private String shortUrl;
    public UrlMappingResponseWithShortUrl(UrlMapping urlMapping){
        this.shortUrl = urlMapping.getShortUrl();
    }
}
