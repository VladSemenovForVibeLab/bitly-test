package ru.semenov.domain.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Builder
@Table(name = UrlMapping.TABLE_NAME)
public class UrlMapping {
    public static final String TABLE_NAME = "url_mapping";
    private static final String URL_MAPPING_ORIGINAL_URL="original_url";
    private static final String URL_MAPPING_SHORT_URL="short_url";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(strategy = "uuid2", name = "uuid")
    @GeneratedValue(generator = "system-uuid")
    private String id;
    @Column(name = URL_MAPPING_ORIGINAL_URL,nullable = false)
    @NotBlank(message = "Оригинальная ссылка не должна быть пустой!")
    private String originalUrl;
    @NotBlank(message = "Сокращенная ссылка не должна быть пустой!")
    private String shortUrl;
}
