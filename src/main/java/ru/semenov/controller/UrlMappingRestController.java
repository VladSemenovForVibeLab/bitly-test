package ru.semenov.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.dto.UrlMappingResponseWithOriginalUrl;
import ru.semenov.dto.UrlMappingResponseWithShortUrl;
import ru.semenov.service.IUrlMappingService;

@RestController
@RequestMapping(UrlMappingRestController.URL_MAPPING_REST_CONTROLLER_URI)
public class UrlMappingRestController {
    public static final String URL_MAPPING_REST_CONTROLLER_URI = "api/v1/urlMapping";
    private final IUrlMappingService urlMappingService;

    public UrlMappingRestController(IUrlMappingService urlMappingService) {
        this.urlMappingService = urlMappingService;
    }

    @PostMapping("/shorten")
    public ResponseEntity<UrlMappingResponseWithShortUrl> shortenUrl(@RequestBody String originalUrl) {
        return urlMappingService.shortenUrl(originalUrl);
    }

    @GetMapping("/{shortenUrl}")
    public ResponseEntity<UrlMappingResponseWithOriginalUrl> redirectToOriginalUrl(@PathVariable("shortenUrl") String shortenUrl){
        return urlMappingService.redirectToOriginalUrl(shortenUrl);
    }
}
