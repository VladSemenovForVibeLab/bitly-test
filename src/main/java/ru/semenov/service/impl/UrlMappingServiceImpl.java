package ru.semenov.service.impl;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.domain.entity.UrlMapping;
import ru.semenov.dto.UrlMappingResponseWithOriginalUrl;
import ru.semenov.dto.UrlMappingResponseWithShortUrl;
import ru.semenov.repository.UrlMappingRepository;
import ru.semenov.service.IUrlMappingService;

import java.util.Optional;
import java.util.Random;

@Service
public class UrlMappingServiceImpl implements IUrlMappingService {
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int SHORT_URL_LENGTH = 7;
    private final UrlMappingRepository urlMappingRepository;

    public UrlMappingServiceImpl(UrlMappingRepository urlMappingRepository) {
        this.urlMappingRepository = urlMappingRepository;
    }

    @Override
    @Transactional
    public ResponseEntity<UrlMappingResponseWithShortUrl> shortenUrl(String originalUrl) {
        UrlMapping build = generateShortenedUrl(originalUrl);
        UrlMapping save = urlMappingRepository.save(build);
        return new ResponseEntity<>(UrlMappingResponseWithShortUrl.builder().shortUrl(save.getShortUrl()).build(), HttpStatus.OK);
    }

    private UrlMapping generateShortenedUrl(String originalUrl) {
        String shortUrl = generateRandomShortUrl();
        return UrlMapping.builder().originalUrl(originalUrl).shortUrl(shortUrl).build();
    }

    private String generateRandomShortUrl() {
        StringBuilder stringBuilder = new StringBuilder(SHORT_URL_LENGTH);
        Random random = new Random();
        for (int i = 0; i < SHORT_URL_LENGTH; i++) {
            int randomIndex = random.nextInt(CHARACTERS.length());
            stringBuilder.append(CHARACTERS.charAt(randomIndex));
        }
        return stringBuilder.toString();
    }

    @Override
    @Transactional
    public ResponseEntity<UrlMappingResponseWithOriginalUrl> redirectToOriginalUrl(String shortUrl) {
        Optional<UrlMapping> urlMappingOptional = urlMappingRepository.findByShortUrl(shortUrl);
        if(urlMappingOptional.isPresent()) {
            UrlMapping urlMapping = urlMappingOptional.get();
            return new ResponseEntity<>(new UrlMappingResponseWithOriginalUrl(urlMapping),HttpStatus.MOVED_PERMANENTLY);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
