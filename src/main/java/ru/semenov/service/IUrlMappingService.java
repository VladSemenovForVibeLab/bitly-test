package ru.semenov.service;

import org.springframework.http.ResponseEntity;
import ru.semenov.dto.UrlMappingResponseWithOriginalUrl;
import ru.semenov.dto.UrlMappingResponseWithShortUrl;

public interface IUrlMappingService {
    ResponseEntity<UrlMappingResponseWithShortUrl> shortenUrl(String originalUrl);

    ResponseEntity<UrlMappingResponseWithOriginalUrl> redirectToOriginalUrl(String shortUrl);
}
