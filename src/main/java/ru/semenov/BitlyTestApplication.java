package ru.semenov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BitlyTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitlyTestApplication.class, args);
	}

}
